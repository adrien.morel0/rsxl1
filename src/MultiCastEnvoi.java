import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class MultiCastEnvoi {
    public static void main(String[] args) {
        try {
            InetAddress ip = InetAddress.getByName("224.0.0.1");
            int port = 7654;
            MulticastSocket socketEmission = new MulticastSocket();
            byte[] contenuMessage;
            DatagramPacket message;
            ByteArrayOutputStream sortie = new ByteArrayOutputStream();
            new DataOutputStream(sortie).writeUTF("Salut");
            contenuMessage = sortie.toByteArray();
            message = new DatagramPacket(contenuMessage, contenuMessage.length, ip, port);
            socketEmission.send(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


