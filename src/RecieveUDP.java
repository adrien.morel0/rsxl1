import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class RecieveUDP {

    public static void main(String[] args) throws IOException {

        byte[] buf = new byte[256];
        DatagramSocket socket = new DatagramSocket(Integer.parseInt(args[0]));
        boolean running = true;

        while (running) {
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);
            System.out.print(new String(packet.getData(), 0, packet.getLength()));
        }
        socket.close();
    }
}

