import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class MultiCastReception {

    public static void main(String[] args) {
        MulticastSocket socketReception = null;
        try {
            InetAddress ip = InetAddress.getByName("224.0.0.1");
            int port = 7654;
            socketReception = new MulticastSocket(port);
            socketReception.joinGroup(ip);
        } catch (IOException e) {
            e.printStackTrace();
        }
        DatagramPacket message;
        byte[] contenuMessage;
        String texte;


        while (true) {
            contenuMessage = new byte[1024];
            message = new DatagramPacket(contenuMessage, contenuMessage.length);
            try {
                socketReception.receive(message);
                texte = (new DataInputStream(new ByteArrayInputStream(contenuMessage))).readUTF();
                System.out.println(texte);
            } catch (Exception exc) {
                System.out.println(exc);
            }
        }
    }
}

