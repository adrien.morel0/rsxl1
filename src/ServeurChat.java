import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class ServeurChat {

    public static final int PORT_SERVICE = 2020;
    private ServerSocket s_Srv;
    ArrayList<ReponseTruc> allClients = new ArrayList<>();

    ServeurChat() throws IOException {
        s_Srv = new ServerSocket(PORT_SERVICE);
    }

    // Réception clients et transfert vers un thread dédié
    private void attenteClient() throws IOException {
        Socket s_Clt;
        while (true) {
            s_Clt = s_Srv.accept();
            System.out.println("Client "+s_Clt.getInetAddress().toString()+" joined the server");
            ReponseTruc rt = new ReponseTruc(s_Clt);
            allClients.add(rt);
            rt.start();
        }
    }

    public static void main(String[] args) throws IOException {
        try {
            ServeurChat srvTruc = new ServeurChat();
            srvTruc.attenteClient();
        }catch (IOException e){
            e.printStackTrace();
        }

    }


    class ReponseTruc extends Thread {
        private Socket s_Client;
        BufferedReader reception;
        PrintStream envoi;
        String msg = null;


        ReponseTruc(Socket sClient) throws IOException {
            this.s_Client = sClient;
            reception = new BufferedReader(new InputStreamReader(s_Client.getInputStream()));
            envoi = new PrintStream(s_Client.getOutputStream());
            // Init des flots de données client ici
        }

        void dialogue(String msg) throws IOException, NullPointerException {
            System.out.println(s_Client.getInetAddress().toString()+": "+msg);
            for (ReponseTruc rt : allClients) {
                if (rt != this)
                    rt.sendMsg(msg);
            }
        }

        void sendMsg(String msg) throws IOException {
            envoi.println(msg);
        }

        public void run() {
            try {
                while ((msg = reception.readLine()) != null) {
                    dialogue(msg);
                }
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }
            allClients.remove(this);
            return;
        }
    }
}
