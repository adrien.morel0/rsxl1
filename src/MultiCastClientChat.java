import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class MultiCastClientChat {
    public static void main(String[] args) {
        String id = args[0];
        InetAddress ip = null;
        try { ip = InetAddress.getByName("224.0.0.1"); } catch (UnknownHostException e) { e.printStackTrace(); }
        int port = 7654;
        new gerantReception(ip,port).start();
        new gerantEnvoi(ip,port,id).start();
    }


    public static class gerantReception extends Thread {
        InetAddress ip;
        int port;

        public gerantReception(InetAddress ip, int port) {
            this.ip = ip;
            this.port = port;
        }


        @Override
        public void run() {
            DatagramPacket message;
            byte[] contenuMessage;
            String texte;
            MulticastSocket socketReception = null;
            try {
                socketReception = new MulticastSocket(port);
            } catch (IOException e) {
                e.printStackTrace();
            }


            while (true) {
                contenuMessage = new byte[1024];
                message = new DatagramPacket(contenuMessage, contenuMessage.length);
                try {
                    socketReception.receive(message);
                    texte = (new DataInputStream(new ByteArrayInputStream(contenuMessage))).readUTF();
                    System.out.println(texte);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class gerantEnvoi extends Thread {
        InetAddress ip;
        int port;
        String id;

        public gerantEnvoi(InetAddress ip, int port,String id) {
            this.ip = ip;
            this.port = port;
            this.id=id;
        }


        @Override
        public void run() {
            Scanner entreeClavier;
            byte[] contenuMessage;
            DatagramPacket message;
            entreeClavier = new Scanner(System.in);
            String texte;
            while (true) {
                try {
                    ByteArrayOutputStream sortie = new ByteArrayOutputStream();
                    MulticastSocket socketEmission = new MulticastSocket();
                    texte = entreeClavier.nextLine();
                    (new DataOutputStream(sortie)).writeUTF(this.id +" : "+texte);
                    contenuMessage = sortie.toByteArray();
                    message = new DatagramPacket(contenuMessage, contenuMessage.length, ip, port);
                    socketEmission.send(message);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}



