import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServBonjour {

    public static void main(String[] args) {
        ServerSocket serverSock = null;
        try {
            serverSock = new ServerSocket(2020);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        while (true) {
            try {
                Socket clientSock = serverSock.accept();

                PrintStream pstream = new PrintStream(clientSock.getOutputStream());
                pstream.println("Bonjour et au revoir\n");
                pstream.close();

                clientSock.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
}
